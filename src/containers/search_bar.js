import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetchWeather} from '../actions/index';

class SearchBar extends Component {

    constructor(props) {
        super(props);

        this.state = {term: ''};

        // cause this in the function will point to the wrong value we need to bind it
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(event) {
        this.setState({term: event.target.value});
    }

    // preventing form submission
    onFormSubmit(event) {
        event.preventDefault();

        // we need to go and fetch weather data
        this.props.fetchWeather(this.state.term);
        this.setState({term: ''});
    }

    render() {
        return (
            // add function to be called when the form is submitted cause we don't need processing made by a browser - post
            <form onSubmit={this.onFormSubmit} className="input-group">
                <input
                    placeholder="Get a five day forecast for your favorite cities"
                    className="form-control"
                    // state of the input should be set by the state in the application
                    value={this.state.term}
                    onChange={this.onInputChange}/>
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">
                        Submit
                    </button>
                </span>
            </form>
        );
    }
}

// facilitates actions created by action creators to be dispatched to middleware
// makes access to out function this.props.fetchWeather inside component
function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchWeather}, dispatch);
}

// passing null because we dont need any state here
// connect component to mapDispatchToProps function
export default connect(null, mapDispatchToProps)(SearchBar);
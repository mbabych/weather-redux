import {FETCH_WEATHER} from '../actions/index';

// redux-promise looks for a promise in the payload property and if does have a promise, and after the promise will resolve it will
// send a new action and send it to reducers
export default function (state = [], action) {

// we need to return new state but not modify the existing state
    switch (action.type) {
        // also possible to use concat syntax but not push as it will return the same object
        case FETCH_WEATHER : {
            return [action.payload.data, ...state];
        }
        default: {
            return state;
        }
    }
}

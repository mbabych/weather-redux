import axios from 'axios';

const API_KEY = '4ca0c68cbbcf7b3006b263b9f3927d4f';
// es6 string template
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

// we need to make the action type consistent across action creators and reducers
export const FETCH_WEATHER = 'FETCH_WEATHER';

// action creator that should return an object that contains type property
export function fetchWeather(city) {
    const url = `${ROOT_URL}&q=${city},us`;
    // making an ajax get request with axios library that returns a promise that should be used by reduxPromise
    const request = axios.get(url);

    // adding a payload with request
    return {
      type: FETCH_WEATHER,
        payload : request
    };
}